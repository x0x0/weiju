package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealComment;

/**
 * <p>
 * 诉求-评论 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealCommentMapper extends BaseMapper<WjAppealComment> {

}
