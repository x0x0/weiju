package com.huixi.microspur.sysadmin.pojo.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 *  修改用户资料接收前端的值
 * @Author 叶秋
 * @Date 2020/6/27 1:22
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="修改用户资料接收前端的值", description="修改用户资料接收前端的值（值都为null的话，就代表不修改呗）")
public class UserProfileDTO {

    @ApiModelProperty(value = "昵称")
    private String nickName;


    @ApiModelProperty(value = "头像对应的URL地址")
    private String headPortrait;

    @ApiModelProperty(value = "性别 改用String，活泼一点。自定义都可以")
    private String sex;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "个性签名（冗余）")
    private String signature;

    @ApiModelProperty(value = "简介")
    private String introduce;


}
