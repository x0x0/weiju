package com.huixi.microspur.web.pojo.entity.dynamic;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态表
 * </p>
 * @Accessors 的意思可见： https://blog.csdn.net/weixin_38229356/article/details/82937420
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_dynamic")
@ApiModel(value="WjDynamic对象", description="动态表")
public class WjDynamic extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "动态id")
    @TableId(value = "dynamic_id", type = IdType.ASSIGN_UUID)
    private String dynamicId;

    @ApiModelProperty(value = "关联的用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "动态的内容（没有标题之类的）")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "动态标签，对应的字典id")
    @TableField("tag_dict_id")
    private String tagDictId;

    @ApiModelProperty(value = "动态所发素材的url地址，多个用逗号隔开")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "动态素材的类型，多个用逗号隔开")
    @TableField("file_type")
    private String fileType;

    @ApiModelProperty(value = "动态素材的大小，多个用逗号隔开(KB)")
    @TableField("file_size")
    private String fileSize;

    @ApiModelProperty(value = "动态 点赞数||赞同数")
    @TableField("endorse_count")
    private Integer endorseCount;

    @ApiModelProperty(value = "动态 评论数")
    @TableField("comment_count")
    private Integer commentCount;

    @ApiModelProperty(value = "动态 浏览量")
    @TableField("browse_count")
    private Integer browseCount;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;


}
