package com.huixi.microspur.web.pojo.dto.appeal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 *
 * @Author 叶秋
 * @Date 2020/6/18 22:49
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "发布诉求需要的值", description = "发布诉求需要的值")
public class WjAppealDTO {

    @ApiModelProperty(value = "诉求对应的用户id", hidden = true)
    private String userId;

    @NotNull(message = "诉求的标题为空")
    @ApiModelProperty(value = "诉求的标题")
    private String title;

    @NotNull(message = "诉求的内容为空")
    @ApiModelProperty(value = "诉求的内容")
    private String content;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "上传的图片")
    private String[] materialList;

}
