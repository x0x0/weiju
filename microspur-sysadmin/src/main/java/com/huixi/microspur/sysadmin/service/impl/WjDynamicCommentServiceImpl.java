package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.mapper.WjDynamicCommentMapper;
import com.huixi.microspur.sysadmin.pojo.entity.dynamic.WjDynamicComment;
import com.huixi.microspur.sysadmin.service.WjDynamicCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态评论表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjDynamicCommentServiceImpl extends ServiceImpl<WjDynamicCommentMapper, WjDynamicComment> implements WjDynamicCommentService {

}
