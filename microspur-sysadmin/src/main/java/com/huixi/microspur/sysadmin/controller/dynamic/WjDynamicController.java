package com.huixi.microspur.sysadmin.controller.dynamic;

import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.sysadmin.pojo.dto.dynamic.WjDynamicPageDTO;
import com.huixi.microspur.sysadmin.pojo.vo.dynamic.DynamicInfoVO;
import com.huixi.microspur.sysadmin.pojo.vo.dynamic.QueryDynamicVO;
import com.huixi.microspur.sysadmin.service.WjDynamicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  后台动态管理
 *
 * @author wuyiliang
 * @date 2020/8/25 11:12
 */
@RestController
@RequestMapping("/dynamic")
@Api(tags = "动态管理")
public class WjDynamicController {

    @Autowired
    private WjDynamicService wjDynamicService;

    /**
     *  动态分页查询
     *
     * @param dto
     * @return
     */
    @ApiOperation(value = "动态分页")
    @PostMapping("/page")
    public Wrapper<PageData<QueryDynamicVO>> page(@RequestBody WjDynamicPageDTO dto) {
        PageData<QueryDynamicVO> pageData = wjDynamicService.listPageDynamic(dto);
        return Wrapper.ok(pageData);
    }

    /**
     *  动态详情
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "动态详情")
    @GetMapping("/{id}")
    public Wrapper<DynamicInfoVO> get(@PathVariable("id") String id) {
        DynamicInfoVO vo = wjDynamicService.get(id);
        return Wrapper.ok(vo);
    }
}
