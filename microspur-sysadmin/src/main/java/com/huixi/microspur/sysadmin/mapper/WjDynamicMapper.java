package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.dynamic.WjDynamic;

/**
 * <p>
 * 动态表 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicMapper extends BaseMapper<WjDynamic> {

}
