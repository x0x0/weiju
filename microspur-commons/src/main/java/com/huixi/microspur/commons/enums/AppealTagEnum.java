package com.huixi.microspur.commons.enums;

/**
 *  诉求的标签
 * @Author 叶秋
 * @Date 2020/7/2 23:27
 * @param
 * @return
 **/
public enum  AppealTagEnum {

    APPEAL_TAG_100(100, "生病中🤧"),
    APPEAL_TAG_101(101, "游戏🎮"),
    APPEAL_TAG_102(102, "篮球⛹️‍♀️⛹️‍♂️"),
    APPEAL_TAG_2333(2333, "bilibili❤"),
    ;

    private Integer id;
    private String value;


    AppealTagEnum(Integer id ,String value) {
        this.id = id;
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public Integer id() {
        return this.id;
    }

    public static AppealTagEnum byIdTagValue(Integer id){

        AppealTagEnum[] values = AppealTagEnum.values();
        for (AppealTagEnum value : values) {
            if(id.equals(value.id)){
                return value;
            }
        }

        return AppealTagEnum.APPEAL_TAG_2333;

    }

}
