package com.huixi.microspur.web.pojo.entity.dynamic;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  动态的点赞表
 * @Author 叶秋 
 * @Date 2020/4/20 21:52
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_dynamic_endorse")
@ApiModel(value="WjDynamicEndorse对象", description="动态点赞表")
public class WjDynamicEndorse extends BaseEntity {


    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "动态点赞表的id")
    @TableId(value = "dynamic_endorse_id", type = IdType.ASSIGN_UUID)
    private String dynamicEndorseId;

    @ApiModelProperty(value = "动态id")
    @TableField(value = "dynamic_id")
    private String dynamicId;

    @ApiModelProperty(value = "点赞人id")
    @TableField(value = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建人id")
    @TableField(value = "create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人id")
    @TableField(value = "update_by")
    private String updateBy;



}
