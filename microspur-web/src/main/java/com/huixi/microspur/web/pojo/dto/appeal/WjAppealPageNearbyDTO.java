package com.huixi.microspur.web.pojo.dto.appeal;

import com.huixi.microspur.commons.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  分页查询附近的诉求 接收前端的参数
 * @Author 叶秋
 * @Date 2020/7/14 18:12
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "分页查询附近的诉求 接收前端的参数")
public class WjAppealPageNearbyDTO {

    @ApiModelProperty(value = "用户id", hidden = true)
    private String userId;

    @ApiModelProperty(value = "模糊搜索内容")
    private String title;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "经度")
    private String longitude;


    @ApiModelProperty(value = "分页参数")
    private PageQuery pageQuery;

}
