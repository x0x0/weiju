package com.huixi.microspur.sysadmin.pojo.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
@Data
@Accessors(chain = true)
@TableName("sys_role_menu")
@ApiModel(value="SysRoleMenu对象", description="")
public class SysRoleMenu{

    private static final long serialVersionUID=1L;

    @TableId("role_id")
    private String roleId;

    @TableField("menu_id")
    private String menuId;


}
