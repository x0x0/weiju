package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.chat.WjChat;

/**
 * <p>
 * 聊天室（聊天列表） Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatMapper extends BaseMapper<WjChat> {

}
