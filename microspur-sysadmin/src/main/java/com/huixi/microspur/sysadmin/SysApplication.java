package com.huixi.microspur.sysadmin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>
 * 启动器
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 **/
@SpringBootApplication
@MapperScan("com.huixi.microspur.sysadmin.mapper")
@EnableCaching
@EnableAsync
@EnableTransactionManagement
public class SysApplication {
    public static void main(String[] args) {
        SpringApplication.run(SysApplication.class, args);
    }
}
