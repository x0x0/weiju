package com.huixi.microspur.commons.errorcode;

/**
 * 错误代码
 *
 * @author 碧海青天夜夜心
 * @date 2020/07/17
 */
public interface IErrorCode {
    /**
     * 获取代码
     *
     * @return {@link String}
     */
    String getCode();

    /**
     * 获取消息
     *
     * @return {@link String}
     */
    String getMessage();
}
