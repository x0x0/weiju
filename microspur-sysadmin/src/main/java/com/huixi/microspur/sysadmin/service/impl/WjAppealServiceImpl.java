package com.huixi.microspur.sysadmin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.sysadmin.mapper.WjAppealMapper;
import com.huixi.microspur.sysadmin.pojo.dto.appeal.WjAppealPageDTO;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealComment;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealEndorse;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealTag;
import com.huixi.microspur.sysadmin.pojo.entity.user.WjUser;
import com.huixi.microspur.sysadmin.pojo.vo.appeal.QueryAppealByIdVo;
import com.huixi.microspur.sysadmin.pojo.vo.appeal.QueryAppealVO;
import com.huixi.microspur.sysadmin.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {

    @Resource
    private WjAppealEndorseService wjAppealEndorseService;

    @Resource
    private WjUserService wjUserService;

    @Resource
    private WjAppealTagService wjAppealTagService;

    @Resource
    private WjAppealCommentService wjAppealCommentService;

    /**
     * 按条件分页查询 诉求
     *
     * @param wjAppealPageDTO
     * @return
     * @Author 叶秋
     * @Date 2020/2/26 21:14
     **/
    @Override
    public PageData listPageAppeal(WjAppealPageDTO wjAppealPageDTO) {
        // 拼接分页参数
        Page<WjAppeal> page = PageFactory.createPage(wjAppealPageDTO.getPageQuery());
        // 拼接查询参数
        LambdaQueryWrapper<WjAppeal> appealLambdaQueryWrapper = Wrappers.<WjAppeal>lambdaQuery()
                .like(StrUtil.isNotBlank(wjAppealPageDTO.getTitle()), WjAppeal::getTitle, wjAppealPageDTO.getTitle())
                .orderByDesc(wjAppealPageDTO.getCreateTime(), WjAppeal::getCreateTime)
                .orderByDesc(wjAppealPageDTO.getEndorseCount(), WjAppeal::getEndorseCount);

        Page<WjAppeal> page1 = page(page, appealLambdaQueryWrapper);
        List<WjAppeal> appealList = page1.getRecords();

        // 获取appealId
        Set<String> appealIdSet = appealList.stream().map(WjAppeal::getAppealId).collect(Collectors.toSet());

        // 获取用户信息
        Set<String> userIdSet = appealList.stream().map(WjAppeal::getUserId).collect(Collectors.toSet());
        List<WjUser> userList = wjUserService.lambdaQuery().in(CollUtil.isNotEmpty(userIdSet), WjUser::getUserId, userIdSet).list();

        // 标签信息
        Map<String, List<WjAppealTag>> appealTagMap = wjAppealTagService.lambdaQuery().in(CollUtil.isNotEmpty(appealIdSet), WjAppealTag::getAppealId, appealIdSet).list().stream().collect(Collectors.groupingBy(WjAppealTag::getAppealId));

        // 评论数
        List<WjAppealComment> appealCommentList = wjAppealCommentService.lambdaQuery().in(CollUtil.isNotEmpty(appealIdSet), WjAppealComment::getAppealId, appealIdSet).list();
        Map<String, Long> appealCommentMap = appealCommentList.stream().collect(Collectors.groupingBy(WjAppealComment::getAppealId, Collectors.counting()));

        // 点赞数
        List<WjAppealEndorse> appealEndorseList = wjAppealEndorseService.lambdaQuery().in(CollUtil.isNotEmpty(appealIdSet), WjAppealEndorse::getAppealId, appealIdSet).list();
        Map<String, Long> appealEndorseMap = appealEndorseList.stream().collect(Collectors.groupingBy(WjAppealEndorse::getAppealId, Collectors.counting()));

        List<QueryAppealVO> list = new ArrayList<>();
        QueryAppealVO queryAppealVO;
        for (WjAppeal wjAppeal : appealList) {
            queryAppealVO = new QueryAppealVO();
            BeanUtil.copyProperties(wjAppeal, queryAppealVO);
            // 用户信息
            WjUser wjUser = userList.stream().filter(e -> e.getUserId().equals(wjAppeal.getUserId())).findAny().orElseGet(WjUser::new);
            queryAppealVO.setNickName(wjUser.getNickName()).setHeadPortrait(wjUser.getHeadPortrait());
            // 标签信息
            queryAppealVO.setAppealTag(appealTagMap.get(wjAppeal.getAppealId()) == null ? Collections.emptyList() : appealTagMap.get(wjAppeal.getAppealId()));
            // 评论数
            queryAppealVO.setCommentCount(appealCommentMap.get(wjAppeal.getAppealId()) == null ? 0 : appealCommentMap.get(wjAppeal.getAppealId()).intValue());
            // 点赞数
            queryAppealVO.setEndorseCount(appealEndorseMap.get(wjAppeal.getAppealId()) == null ? 0 : appealEndorseMap.get(wjAppeal.getAppealId()).intValue());
            list.add(queryAppealVO);
        }
        PageData pageData = new PageData();
        BeanUtil.copyProperties(page1, pageData);
        pageData.setRecords(list);
        return pageData;
    }

    @Override
    public QueryAppealByIdVo appealById(String appealId) {
        QueryAppealByIdVo queryAppealByIdVo = new QueryAppealByIdVo();
        // 查询单个诉求信息
        WjAppeal wjAppeal = getById(appealId);
        BeanUtil.copyProperties(wjAppeal, queryAppealByIdVo);

        // 查询用户信息
        WjUser wjUser = wjUserService.lambdaQuery().eq(StrUtil.isNotBlank(wjAppeal.getUserId()), WjUser::getUserId, wjAppeal.getUserId()).one();
        queryAppealByIdVo.setNickName(wjUser.getNickName()).setHeadPortrait(wjUser.getHeadPortrait());
        // 查询标签信息
        List<WjAppealTag> appealTagList = wjAppealTagService.lambdaQuery().eq(StrUtil.isNotBlank(appealId), WjAppealTag::getAppealId, appealId).list();
        queryAppealByIdVo.setAppealTag(appealTagList);
        // 评论数
        List<WjAppealComment> appealCommentList = wjAppealCommentService.lambdaQuery().eq(StrUtil.isNotBlank(appealId), WjAppealComment::getAppealId, appealId).list();
        int commentCount = appealCommentList.size();
        queryAppealByIdVo.setCommentCount(commentCount);
        // 评论用户头像
        List<String> commentUserHeadPortrait = null;
        if (0 >= commentCount) {
            commentUserHeadPortrait = Collections.emptyList();
        } else {
            // 提取评论的用户id
            Set<String> userIdSet = appealCommentList.stream().map(WjAppealComment::getUserId).collect(Collectors.toSet());
            // 获取评论用户的头像
            List<WjUser> wjUserList = wjUserService.lambdaQuery().in(CollUtil.isNotEmpty(userIdSet), WjUser::getUserId, userIdSet).list();
            commentUserHeadPortrait = wjUserList.stream().map(e -> e.getHeadPortrait()).collect(Collectors.toList());
        }
        queryAppealByIdVo.setCommentUserHeadPortrait(commentUserHeadPortrait);
        // 点赞数
        int endorseCount = wjAppealEndorseService.lambdaQuery().eq(StrUtil.isNotBlank(appealId), WjAppealEndorse::getAppealId, appealId).list().size();
        queryAppealByIdVo.setEndorseCount(endorseCount);
        return queryAppealByIdVo;
    }

}
