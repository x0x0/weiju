package com.huixi.microspur.web.pojo.dto.appeal;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 *  修改诉求 的接收实体
 * @Author 叶秋 
 * @Date 2020/6/30 22:35
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "修改诉求 的接收实体", description = "修改诉求 的接收实体")
public class UpdateAppealDTO {

    @NotNull(message = "诉求id为空")
    @ApiModelProperty(value = "诉求id", required = true)
    private String appealId;

    @ApiModelProperty(value = "诉求对应的用户id", hidden = true)
    private String userId;

    @NotNull(message = "诉求的标题为空")
    @ApiModelProperty(value = "诉求的标题")
    private String title;

    @NotNull(message = "诉求的内容为空")
    @ApiModelProperty(value = "诉求的内容")
    private String content;

    @ApiModelProperty(value = "标签名字，暂时由用户自定义。前端就是让用户自己写标签，不超过3个，可以是3个")
    private String[] tagName;

    @ApiModelProperty(value = "上传的图片，没有修改图片的话就什么都不传")
    private String[] materialList;

    @ApiModelProperty(value = "纬度，没有修改就什么都不传")
    private String lat;

    @ApiModelProperty(value = "经度，没有修改就什么都不传")
    private String lng;




}
