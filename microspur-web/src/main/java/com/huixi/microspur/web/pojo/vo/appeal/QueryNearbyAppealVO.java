package com.huixi.microspur.web.pojo.vo.appeal;

import com.huixi.microspur.web.pojo.entity.appeal.WjAppealMaterial;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealTag;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.geo.Metric;
import org.springframework.data.geo.Point;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 附近诉求类 诉求需要多个表的数据合并才能合成完整数据，故此一个包装
 * @Author 叶秋
 * @Date 2020/3/5 23:11
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class QueryNearbyAppealVO implements Serializable {


    // 诉求类

    @ApiModelProperty(value = "诉求的id")
    private String appealId;

    @ApiModelProperty(value = "诉求对应的用户id")
    private String userId;

    @ApiModelProperty(value = "诉求的标题")
    private String title;

    @ApiModelProperty(value = "诉求的内容")
    private String content;

    @ApiModelProperty(value = "诉求点赞量")
    private Integer endorseCount;

    @ApiModelProperty(value = "诉求的评论量")
    private Integer commentCount;

    @ApiModelProperty(value = "诉求浏览量")
    private Integer browseCount;

    @ApiModelProperty(value = "是否点赞")
    private Boolean isEndorse;

    @ApiModelProperty(value = "创建时间")
    public LocalDateTime createTime;

//    距离信息

    @ApiModelProperty(value = "距离")
    private Double distance;

    @ApiModelProperty(value = "距离单位")
    private Metric metric;

    @ApiModelProperty(value = "经纬度信息")
    private Point point;


//    用户类

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像对应的URL地址")
    private String headPortrait;


    @ApiModelProperty(value = "诉求相关的素材")
    private List<WjAppealMaterial> appealMaterial;

    @ApiModelProperty(value = "标签的名字（冗余）")
    private List<WjAppealTag> appealTag;


}
