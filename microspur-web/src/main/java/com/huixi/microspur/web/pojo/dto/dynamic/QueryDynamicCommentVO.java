package com.huixi.microspur.web.pojo.dto.dynamic;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 *  动态评论 封装好传给前端
 * @Author 叶秋
 * @Date 2020年8月8日21:19:45
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="动态-评论封装", description="封装好一些必要的参数就行")
public class QueryDynamicCommentVO {

    @ApiModelProperty(value = "动态评论的id")
    private String dynamicCommentId;

    @ApiModelProperty(value = "是否是评价的评论（这里要写被评论的id，写了就是，没写就是普通的评论）")
    private String aboutComment;

    @ApiModelProperty(value = "动态id")
    private String dynamicId;

    @ApiModelProperty(value = "评论的内容")
    private String content;

    @ApiModelProperty(value = "评论所加的贴图（只能是一张）")
    private String url;

    @ApiModelProperty(value = "创建时间")
    public LocalDateTime createTime;


    // 用户部分

    @ApiModelProperty(value = "评论人的id")
    private String userId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像对应的URL地址")
    private String headPortrait;

}
