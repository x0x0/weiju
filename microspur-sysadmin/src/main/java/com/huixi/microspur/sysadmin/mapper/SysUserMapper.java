package com.huixi.microspur.sysadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.sys.SysUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
