package com.huixi.microspur.web.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.huixi.microspur.commons.enums.DelFlagEnum;
import com.huixi.microspur.commons.enums.EnableFlagEnum;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 公共字段，自动填充值
 */
@Component
public class FieldMetaObjectHandler implements MetaObjectHandler {

    private final static String CREATE_TIME = "createTime";
    private final static String UPDATE_TIME = "updateTime";
    private final static String DEL_FLAG = "delFlag";
    private final static String ENABLE_FLAG = "enableFlag";

    @Override
    public void insertFill(MetaObject metaObject) {

        LocalDateTime localDateTime = LocalDateTime.now();

        //创建时间
        setFieldValByName(CREATE_TIME, localDateTime, metaObject);

        //更新时间
        setFieldValByName(UPDATE_TIME, localDateTime, metaObject);

        //删除标识
        setFieldValByName(DEL_FLAG, DelFlagEnum.NORMAL.value(), metaObject);

        //是否启用标识
        setFieldValByName(ENABLE_FLAG, EnableFlagEnum.ENABlED.value(), metaObject);

    }

    @Override
    public void updateFill(MetaObject metaObject) {

        LocalDateTime localDateTime = LocalDateTime.now();

        //更新时间
        setFieldValByName(UPDATE_TIME, localDateTime, metaObject);
    }
}