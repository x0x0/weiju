package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.sysadmin.pojo.dto.dynamic.WjDynamicPageDTO;
import com.huixi.microspur.sysadmin.pojo.entity.dynamic.WjDynamic;
import com.huixi.microspur.sysadmin.pojo.vo.dynamic.DynamicInfoVO;
import com.huixi.microspur.sysadmin.pojo.vo.dynamic.QueryDynamicVO;

/**
 * <p>
 * 动态表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicService extends IService<WjDynamic> {


    /**
     * 判断是否是自己所发的
     * @Author 叶秋
     * @Date 2020/7/22 14:56
     * @param userId    用户id
     * @param dynamicId 动态id
     * @return java.lang.Boolean
     **/
    Boolean judgeIsMeDynamic(String userId, String dynamicId);

    /**
     *  分页查询动态
     * @Author 叶秋
     * @Date 2020/4/22 11:38
     * @param wjDynamicPageVO
     * @return java.util.List<com.huixi.microspur.web.pojo.vo.dynamic.QueryDynamicVO>
     **/
    PageData<QueryDynamicVO> listPageDynamic(WjDynamicPageDTO wjDynamicPageVO);


    /**
     *  分页查询自己的诉求
     * @Author 叶秋
     * @Date 2020/7/16 21:05
     * @param wjDynamicPageVO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    PageData<QueryDynamicVO> listPageMyDynamic(WjDynamicPageDTO wjDynamicPageVO);

    /**
     *  动态详情
     * @param id
     * @return
     */
    DynamicInfoVO get(String id);
}
